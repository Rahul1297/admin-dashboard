import React, { Component } from 'react';
import './CustomTable.scss';


class CustomTable extends Component {
    
    render() {
         return ( 
        <div >
            <div className="table-wrapper">
                <div className="table-title">
                    <div className="row">
                        <div className="col-sm-3">
                            <h2>Manage Employees</h2>
                        </div>
                        <div className="col-sm-3">
                            <a href="#addEmployeeModal" className="btn btn-success" data-toggle="modal"><i className="material-icons">&#xE147;</i> <span>Add New Employee</span></a>						
                        </div>
                        <div class="col-sm-3">
                            <div class="search-box">
                                <i class="material-icons">&#xE8B6;</i>
                                <input type="text" onChange={(event) => this.props.searchData(event.target.value)} class="form-control" placeholder="Search&hellip;"/>
                            </div>
                        </div>
                         <div className="col-sm-3">
                            <a href="#exportEmployeeModal" onClick={()=>{}} className="btn btn-success-E" data-toggle="modal"><i className="material-icons">&#xE24D;</i> <span>Export</span></a>						
                        </div>
                    </div>
                </div>

        <div className="table-section">
                {this.props.jsonRows && this.props.jsonRows.length>0 ? <table cellpadding="100" cellspacing="100" className="table table-striped table-hover">
                    <thead>
                        <tr>
                            {/* <th>
                                <span className="custom-checkbox">
                                    <input type="checkbox" id="selectAll"/>
                                    <label for="selectAll"></label>
                                </span>
                            </th> */}
                            <th>Sr.No</th>
                            {this.props.jsonArray.map((keys) => {
                                return <th>{keys}</th>
                            })}
                            <th>Actions</th>
                        </tr>
                    </thead> 
                    <tbody>
                        {/* iterate rows */ }
                        {
                            this.props.jsonRows.map( (ele,index) => {
                                return (<tr>
                                        {/* <td>
                                            <span className="custom-checkbox">
                                                <input type="checkbox" id="checkbox1" name="options[]" value="1"/>
                                                <label for="checkbox1"></label>
                                            </span>
                                        </td> */}
                                        <td>{index+1}</td>
                                        {
                                            Object.keys(ele).map((key) => {
                                                return <td value={key}> {ele[key]} </td>
                                            })
                                        }
                                        <td style={{display:'flex'}}>
                                            <a onClick={ () => {this.props.sendEditData(ele,index)} } href="#editEmployeeModal" className="edit" data-toggle="modal"><i className="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                                            <a onClick={ () => {this.props.sendDeleteData(index)} } href="#deleteEmployeeModal" className="delete" data-toggle="modal"><i className="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                                        </td>
                                    </tr>
                                )
                            })
                        }   
                    </tbody>
                </table>  :<h4>No Results Found</h4>}       
                </div>       
            </div>
        </div>
      );
    }
  }

  export default CustomTable;