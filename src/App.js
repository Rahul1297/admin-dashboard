import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import csvUpload from './containers/csvUpload';
import crudCsv from './containers/crudCsv';




class App extends Component {
  
  render() {
    return (
    <Router>
          
          <Switch>
              <Route exact path='/' component={csvUpload} />
              <Route exact path='/crudCsv' component={crudCsv} />
          </Switch>
      </Router>

    );
  }
}

export default App;