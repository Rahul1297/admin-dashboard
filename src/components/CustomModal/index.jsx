import React, { Component } from 'react';
import './CustomModal.scss';

class CustomModal extends Component{

constructor(props){
	super(props)
	this.state={
	 jsonArray:[],
	 newObject:null,
	 elementData:[],
	 checkboxValue:[]
	};
}

 componentWillMount(){
	this.setState({jsonArray:this.props.jsonArray});
	let checkboxValue=[];
	this.props.jsonArray.forEach((ele,i)=>{
		checkboxValue[i]=true;
	})

	this.setState({checkboxValue})
 }
 componentWillReceiveProps(nextProps){
	 if(nextProps.elementData){
		 this.setState({elementData:nextProps.elementData});
	 }
 }
handleChange(index,newValue){
	 let newData=this.state.elementData;
	 newData[index]=newValue;
	 this.setState({elementData:newData})
 }

 submitData(){
	let checkData=this.state.elementData;
	let count=0;
	console.log('aaa',checkData)
	
	checkData.forEach((element,i) => {
		if(checkData[i].trim()===''){
		count++;
		}
	});
	if((this.state.elementData && this.state.elementData.length ===0) || count===this.state.jsonArray.length){
		alert('Please enter data');
	}else{
	this.props.submitData(this.state.elementData);
	document.getElementById('closeAddModal').click();
	document.getElementById('closeEditModal').click();
	}
}

JSONtoCsv(){
	const rectifiedArray=this.state.checkboxValue.map((ele,i)=>{
		return ele?1:0;
	})
	this.props.JSONtoCsv(rectifiedArray);
	document.getElementById('closeExportModal').click();
}

deleteRow(){
	this.props.deleteRow();
	document.getElementById('closeDeleteModal').click();
}

handleCheckboxChange(index){
	let updateValues=this.state.checkboxValue;
	if(updateValues[index]){
		updateValues[index]=false;
	}else{
		updateValues[index]=true;
	}
	this.setState({checkboxValue:updateValues})
}

    render(){	
        return(
			<div>
			{/* ADD Modal HTML */}
			{this.state.jsonArray && this.state.jsonArray.length>0 ? (<div>
			<div id="addEmployeeModal" class="modal fade">

				<div class="modal-dialog">
					<div class="modal-content">
						<form>
							<div class="modal-header">						
								<h4 class="modal-title">Add Employee</h4>
							</div>
							<div class="modal-body">	
								<div class="form-group">
								{this.props.jsonArray.map((keys,index)=>{
								return (
								<div>
								<label>{keys}</label>
								<input type="text" value={this.state.elementData&& this.state.elementData[index]} onChange={(event)=>this.handleChange(index,event.target.value)}  class="form-control" required/>
								</div>
								)})}
								
								</div>
											
								
												
							</div>
							<div class="modal-footer">
								<input type="button" id='closeAddModal' class="btn btn-info" data-dismiss="modal" value="Cancel"/>
								<input type="button" class="btn btn-success" value="Add" onClick={()=>this.submitData()}/>
							</div>
						</form>
					</div>
				</div>
			</div>
			{/* <!-- Edit Modal HTML --> */}
			<div id="editEmployeeModal" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<form>
							<div class="modal-header">						
								<h4 class="modal-title">Edit Employee</h4>
							</div>
							<div class="modal-body">					
							
								<div class="form-group">
								{this.state.jsonArray.map((keys,index)=>{
								return (
									<div>
									<label>{keys}</label>
									<input type="text" value={this.state.elementData && this.state.elementData[index]} onChange={(event)=>this.handleChange(index,event.target.value)} class="form-control" required/>
									</div>
								)})}
								
								</div>
							
								
							</div>
							<div class="modal-footer">
								<input type="button" id="closeEditModal" class="btn btn-info" data-dismiss="modal" value="Cancel"/>
								<input type="button" class="btn btn-success" value="Save" onClick={()=>this.submitData()}/>
							</div>
						</form>
					</div>
				</div>
			</div>
			{/* <!-- Delete Modal HTML --> */}
			<div id="deleteEmployeeModal" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<form>
							<div class="modal-header">						
								<h4 class="modal-title">Delete Employee</h4>
							</div>
							<div class="modal-body">					
								<p>Are you sure you want to delete these Records?</p>
								<p class="text-warning"><small>This action cannot be undone.</small></p>
							</div>
							<div class="modal-footer">
								<input type="button" id="closeDeleteModal" class="btn btn-info" data-dismiss="modal" value="Cancel"/>
								<input type="button" class="btn btn-danger" value="Delete" onClick={()=>this.deleteRow()}/>
							</div>
						</form>
					</div>
				</div>
			</div>
			{/* <!-- Export Modal HTML --> */}
			<div id="exportEmployeeModal" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<form>
							<div class="modal-header">						
								<h4 class="modal-title">Download CSV File</h4>
							</div>
							<div class="modal-body">					
							
								<div class="form-group">
								{this.state.jsonArray.map((keys,index)=>{
								return (
									<div>
									<span className="custom-checkbox">
                                    <input type="checkbox" id="checkbox1" name="options[]" checked={this.state.checkboxValue[index]} onChange={()=>{this.handleCheckboxChange(index)}}/>

                                    <label for="checkbox1"></label>
                                    </span>
									<label>{keys}</label>
									</div>
								)})}
								
								</div>
							
								
							</div>
							<div class="modal-footer">
								<input type="button" id="closeExportModal" class="btn btn-info" data-dismiss="modal" value="Cancel"/>
								<input type="button" class="btn btn-success" onClick={()=>this.JSONtoCsv()} value="Download" />
							</div>
						</form>
					</div>
				</div>
			</div>
			</div>) : null}
			</div>
        )
    }
}
export default CustomModal;