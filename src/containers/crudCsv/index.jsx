import React, {Component} from 'react';
import CustomModal from '../../components/CustomModal';
import CustomTable from '../../components/CustomTable';
import Papa from 'papaparse';



class crudCsv extends Component{

  constructor(props) {
    super(props);
    this.state={
      jsonInput:[],
      heading: null,
      userData: null,
      userData2: null,
      elementData: null,
      selectedRow:-1,
    }
    this.setNewData=this.setNewData.bind(this);
  }  
  

  componentWillMount(){
    this.fetchData();
  }

  fetchData(){
    let jsonInput=[];
    jsonInput.push(JSON.parse(localStorage.getItem('json data')).data);
    const array=jsonInput[0][0]
    const rows=jsonInput[0];
    rows.splice(0,1);
    rows.splice(-1,1)
    this.setState({heading:array, userData: rows, userData2:rows, jsonInput});
  } 

  handleNewData(data){
    this.state.heading.forEach((element,index) => {
        if(typeof(data[index])==='undefined'){
          data[index]='';
        }
    });
    let updateList=this.state.userData;
    if(this.state.selectedRow===-1){
      updateList.push(data);
    }else{
      updateList[this.state.selectedRow]=data;
    }
    this.setNewData(updateList)
  }

  deleteRow(index){
    let updateList=this.state.userData;
    
    updateList.splice(index, 1);
    this.setNewData(updateList)
  }

  filterData =(keyword)=>{

    return new Promise((resolve,reject)=>{
      let newList=[];
      if(keyword.trim()===''){
        newList=this.state.userData2;
      }
      else{
        let updateList=this.state.userData2;
        
        updateList.forEach((element)=>{
          var keywordMatched=false;
            element.forEach((ele)=>{
              let val=ele.indexOf(keyword) !==-1? true: false
              if(val){
                keywordMatched=true;
              }
            })
            if(keywordMatched){
              newList.push(element);
            }
        });
      }
       
      resolve(newList)
    })
    
  }
  getData = async(keyword)=>{
    let newList=await this.filterData(keyword)
    this.setState({userData:newList,selectedRow:-1})
  }
  setNewData(data){ 
    this.setState({userData:data,userData2:data,selectedRow:-1})
  }

  convertToCsv(data){
      return Papa.unparse(data || this.state.userData, {
      quotes: false, //or array of booleans
      quoteChar: '"',
      escapeChar: '"',
      delimiter: ",",
      header: false,
      newline: "\r\n",
      skipEmptyLines: false, //or 'greedy',
      columns: this.state.heading //or array of strings
    });
  }


  exportCSVFile(selected) {

    const fileTitle='Updated CSV';
    console.log(selected);
    //Simulating header selections:
    //You should know which item positions to omit
    let newHeading = this.state.heading.filter((d,i) => {return selected[i]===1})
    // console.log(newHeading);

    let newUserData = this.state.userData.map(user => {return user.filter((d,i) => {return selected[i]===1})}) 

    // console.log(newUserData);

    var csv = this.convertToCsv([newHeading, ...newUserData]);

    var exportedFilenmae = (fileTitle || 'export') + '.csv';

    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", exportedFilenmae);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}


render(){
    return(
        <div>

        <CustomTable 
        sendEditData={ (elementData,index) => {
          this.setState({elementData:elementData,selectedRow:index});
        }} 
        sendDeleteData={ (index) => this.setState({selectedRow:index})} 
        jsonRows={this.state.userData} 
        jsonArray={this.state.heading}
        searchData={(keyword)=>this.getData(keyword)}
        />
      

        <CustomModal 
        elementData={this.state.elementData} 
        jsonArray={this.state.heading}
        submitData={(data)=>this.handleNewData(data)}
        deleteRow={()=>this.deleteRow(this.state.selectedRow)}
        JSONtoCsv={(selectedArray)=>this.exportCSVFile(selectedArray)}
        /> 
        
        
      </div>  
    )
}
}
export default crudCsv;