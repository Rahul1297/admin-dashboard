import React from 'react';
import './csvUpload.scss';
import ReactUploadFile from 'react-upload-file';
import papaparser from 'papaparse';

class csvUpload extends React.Component {
    constructor(props) {
      super(props);
      this.state={
        files:null,
        fileUploading:false
      }
   
    }  
    
 render() {
    let _this=this;
    const options = {
    baseUrl: 'api/upload',
    multiple: false,
    accept: 'csv/*',

    didChoose: (files) => {
      if(files.length){
        _this.setState({
          fileUploading: true
        })
       console.log('Image File',files);
       let config = {
         complete : (JSONfile)=> {
          localStorage.setItem('json data', JSON.stringify(JSONfile));
          _this.setState({
            fileUploading: false
          })
          this.props.history.push('/crudCsv');
         }
       }
       papaparser.parse(files[0], config)
      }
      
   
    },
  };



      return (
        <div >
          <div className="upload-csv">
            <h3 className="heading">Upload CSV File Here</h3>
            <div className="upload">
             <ReactUploadFile
              options={options} 
              chooseFileButton={<button className="choose-btn">Choose File</button>}
               uploadFileButton={<button className="upload-btn">{this.state.fileUploading?'UPLOADING.....':'UPLOAD'}</button> }
                />
              </div>
           </div>     
        </div>
      );
    }
  }
  export default csvUpload;