import React, { Component } from 'react';
import './CustomModal.scss';

class CustomModal extends Component{
    render(){
		// const arr=this.props.inputJSON[0][0];
		// console.log('Modal',this.props.inputJSON);
		// const r = this.props.inputJSON[0];
		// r.splice(0,1);
		// r.splice(-1,1);
        return(
			<div>
			{/* ADD Modal HTML */}
			<div id="addEmployeeModal" classNames="modal fade">
				<div classNames="modal-dialog">
					<div classNames="modal-content">
						<form>
							<div classNames="modal-header">						
								<h4 classNames="modal-title">Add Employee</h4>
							</div>
							<div classNames="modal-body">	
								{
									this.props.jsonRows.map( (ele,index) => {
									return (
										<div classNames="form-group">
										{
											this.props.jsonArray.map((keys) => {
												return (
													<div>
														<label>{keys}</label>
														<input type="text" classNames="form-control" required/>
													</div>
												)
											})
										}
										</div>
								)})
							}				
								
												
							</div>
							<div classNames="modal-footer">
								<input type="button" classNames="btn btn-default" data-dismiss="modal" value="Cancel"/>
								<input type="submit" classNames="btn btn-success" value="Add"/>
							</div>
						</form>
					</div>
				</div>
			</div>
			{/* <!-- Edit Modal HTML --> */}
				<div id="editEmployeeModal" classNames="modal fade">
				<div classNames="modal-dialog">
					<div classNames="modal-content">
						<form>
								<div classNames="modal-header">						
								<h4 classNames="modal-title">Edit Employee</h4>
								</div>
								<div classNames="modal-body">					
								{
									this.props.jsonRows.map((ele,index)=>{
										return (
												<div classNames="form-group">
												{
													this.props.jsonArray.map((keys,index)=>{
												return (
												<div>
													<label>{keys}</label>
													<input type="text" classNames="form-control" required/>
												</div>
												)})}
								
												</div>
									)})
								}
								
							</div>
							<div classNames="modal-footer">
								<input type="button" classNames="btn btn-default" data-dismiss="modal" value="Cancel"/>
								<input type="submit" classNames="btn btn-info" value="Save"/>
							</div>
						</form>
					</div>
				</div>
			</div>
			{/* <!-- Delete Modal HTML --> */}
			 <div id="deleteEmployeeModal" classNames="modal fade">
				<div classNames="modal-dialog">
					<div classNames="modal-content">
						<form>
							<div classNames="modal-header">						
								<h4 classNames="modal-title">Delete Employee</h4>
							</div>
							<div classNames="modal-body">					
								<p>Are you sure you want to delete these Records?</p>
								<p classNames="text-warning"><small>This action cannot be undone.</small></p>
							</div>
							<div classNames="modal-footer">
								<input type="button" classNames="btn btn-default" data-dismiss="modal" value="Cancel"/>
								<input type="submit" classNames="btn btn-danger" value="Delete"/>
							</div>
						</form>
					</div>
				</div>
			</div>
			</div>
        )
    }
}
export default CustomModal;